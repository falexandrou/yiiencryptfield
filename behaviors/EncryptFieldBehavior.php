<?php
/**
 * @see CActiveRecordBehavior
 */
class EncryptFieldBehavior extends CActiveRecordBehavior
{
    const DECRYPTED_SUFFIX = '_decrypted';

    /**
     * @var array $fields which are the fields that are getting encrypted/decrypted
     * @example field => configuration
     */
    public $fields=array();

    /**
     * @var array the decrypted values that are used as a temp cache
     */
    private $_decryptedValues=array();

    /**
     * @param CEvent the onBeforeSave CActiveRecordBehavior event
     * @return boolean
     */
    public function beforeSave($event)
    {
        foreach ($this->fields as $field => $config) {
            $this->_decryptedValues[$field] = $this->owner->{$field};
            $this->owner->{$field} = $this->getEncryptExpression($field, $config);
        }

        return parent::beforeSave($event);
    }

    /**
     * @param CEvent the onAfterSave CActiveRecordBehavior event
     * @return void
     */
    public function afterSave($event)
    {
        foreach ($this->fields as $field => $config)
            $this->owner->{$field} = isset($this->_decryptedValues[$field]) ? $this->_decryptedValues[$field] : null;
        parent::afterSave($event);
    }

    /**
     * @param CEvent the onBeforeFind CActiveRecordBehavior event
     * @return boolean
     */
    public function beforeFind($event)
    {
        $selects = array();
        foreach ($this->fields as $field => $config) {
            $selects[] = $this->getDecryptExpression($field, $config);
        }

        $criteria = $this->owner->getDbCriteria();
        $criteria->select .= ", ".implode(', ', $selects);

        return parent::beforeFind($event);
    }

    /**
     * @param string the field to encrypt
     * @param array|EncryptFieldConfig the configuration for encrypting / decrypting
     * @return CDbExpression the expression to use for encrypting a field
     */
    protected function getEncryptExpression($field, $config)
    {
        $config=$this->getConfig($config);
        $value = $this->owner->{$field};
        return new CDbExpression("{$config->encryptFunction}('{$value}', {$config->saltExpression})");
    }

    /**
     * @param string the field to encrypt
     * @param array|EncryptFieldConfig the configuration for encrypting / decrypting
     * @return CDbExpression the expression to use for decrypting a field
     */
    protected function getDecryptExpression($field, $config)
    {
        $config=$this->getConfig($config);
        return new CDbExpression("{$config->decryptFunction}($field, {$config->saltExpression}) AS `{$field}`");
    }

    /**
     * @param array the config as an array
     * @return EncryptFieldConfig
     */
    protected function getConfig($config)
    {
        if (!$config instanceof EncryptFieldConfig)
            $config = Yii::createComponent(CMap::mergeArray(array('class' => 'EncryptFieldConfig'), $config));
        return $config;
    }
}

/**
 * @see CComponent
 */
class EncryptFieldConfig extends CComponent
{
    const AES_ENCRYPT   = 'AES_ENCRYPT';
    const AES_DECRYPT   = 'AES_DECRYPT';

    const DES_ENCRYPT   = 'DES_ENCRYPT';
    const DES_DECRYPT   = 'DES_DECRYPT';

    /**
     * @var string the hash function to use
     */
    public $hashFunction = self::AES_ENCRYPT;

    /**
     * @var CDbExpression the CDbExpression to use for salting
     */
    public $saltExpression;

    /**
     * @see CComponent::init()
     */
    public function init()
    {
        if ($this->hashFunction === null || !in_array($this->hashFunction, static::optsHashFunctions()))
            throw new CException('The hash function should be a two-way hash function available in EncryptFieldConfig::optsHashFunctions()');

        if (!$this->saltExpression instanceof CDbExpression)
            throw new CException('The salt expression should be an instance of CDbExpression');

        parent::init();
    }

    /**
     * @return string the encrypt function to use
     */
    public function getEncryptFunction()
    {
        return $this->hashFunction;
    }

    /**
     * @return string the decrypt function to use
     */
    public function getDecryptFunction()
    {
        $mapping=static::optsEncryptDecryptMapping();
        if (!isset($mapping[$this->hashFunction]))
            throw new CException('There is not a decrypt function set for this particular method');
        return $mapping[$this->hashFunction];
    }

    /**
     * @return array the list of hash functions available
     */
    public static function optsHashFunctions()
    {
        return array(
            self::AES_ENCRYPT,
            self::DES_ENCRYPT,
        );
    }

    /**
     * @return array the key=>value mapping of encrypting / decrypting functions
     */
    protected static function optsEncryptDecryptMapping()
    {
        return array(
            self::AES_ENCRYPT   => self::AES_DECRYPT,
            self::DES_ENCRYPT   => self::DES_DECRYPT,
        );
    }
}